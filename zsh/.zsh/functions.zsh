# update function

function update() {
    local update="brew update && brew upgrade && brew cleanup;"
    local gem="gem update;"
    local npmw="sudo npm update -g;"
    local pip2="pip3 list --outdated --format=freeze | grep -v '^\-e' | cut -d = -f 1  | xargs -n1 pip3 install -U --user"
    sh -c $update ; sh -c $gem ; sh -c $npmw; sh -c $pip2
}

function git_prompt_info() {
  ref=$(git symbolic-ref HEAD 2> /dev/null) || return
  echo "$ZSH_THEME_GIT_PROMPT_PREFIX${ref#refs/heads/}$ZSH_THEME_GIT_PROMPT_SUFFIX"
}